# This is a multistage build.

# Helper image for installing tools we need
# hadolint ignore=DL3007
FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:0.0.1 as installer

ARG GOLANG_VER_SUM="1.14:6d643e46ad565058c7a39dac01144172ef9bd476521f42148be59249e4b74389"
ARG GOLANGCI_LINT_VERSION="1.23.6"
ARG GORELEASER_VERSION="v0.127.0"
ARG DEP_VERSION="v0.5.4"

SHELL ["/bin/bash", "-eufo", "pipefail", "-c"]

# hadolint ignore=DL3003,DL3018,DL4006
RUN apk -qq --no-cache add \
		tar \
		xz \
		gzip \
		binutils \
		bash gcc musl-dev openssl go \
	# install golang
	&& wget -qqO go.tgz "https://golang.org/dl/go${GOLANG_VER_SUM%%:*}.src.tar.gz" \
	&& ( echo "${GOLANG_VER_SUM##*:}  go.tgz" | sha256sum -cs ) \
	&& tar -C /tmp/ -xzf go.tgz \
	&& rm -f go.tgz \
	&& ( cd /tmp/go/src ; ./make.bash ) \
	&& rm -rf /tmp/go/pkg/bootstrap /tmp/go/pkg/obj \
	# install golangci-lint
	&& wget -qq "https://github.com/golangci/golangci-lint/releases/download/v${GOLANGCI_LINT_VERSION}/golangci-lint-${GOLANGCI_LINT_VERSION}-checksums.txt" \
		    "https://github.com/golangci/golangci-lint/releases/download/v${GOLANGCI_LINT_VERSION}/golangci-lint-${GOLANGCI_LINT_VERSION}-linux-amd64.tar.gz" \
		&& grep "golangci-lint-${GOLANGCI_LINT_VERSION}-linux-amd64.tar.gz" "golangci-lint-${GOLANGCI_LINT_VERSION}-checksums.txt" | sha256sum -cs \
		&& tar --strip-components=1 -C /usr/local/bin -zxf "golangci-lint-${GOLANGCI_LINT_VERSION}-linux-amd64.tar.gz" "golangci-lint-${GOLANGCI_LINT_VERSION}-linux-amd64/golangci-lint" \
	# install goreleaser
	&& wget -qq "https://github.com/goreleaser/goreleaser/releases/download/${GORELEASER_VERSION}/goreleaser_checksums.txt" \
		    "https://github.com/goreleaser/goreleaser/releases/download/${GORELEASER_VERSION}/goreleaser_Linux_x86_64.tar.gz" \
		&& grep Linux_x86_64 goreleaser_checksums.txt | sha256sum -cs \
		&& tar -C /usr/local/bin -zxf "goreleaser_Linux_x86_64.tar.gz" "goreleaser" \
	# install dep
	&& wget -qq "https://github.com/golang/dep/releases/download/${DEP_VERSION}/dep-linux-amd64.sha256" \
		    "https://github.com/golang/dep/releases/download/${DEP_VERSION}/dep-linux-amd64" \
		&& sed 's|  .*/|  |' dep-linux-amd64.sha256 | sha256sum -cs \
		&& mv dep-linux-amd64 /usr/local/bin/dep \
	# install else
	&& echo "done"

# This is our final image, based on base image
FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:0.0.1

LABEL version="0.0.3" \
      maintaner="Ilya A. Frolov <if+gitlab@solas.is>" \
      description="Image we use as a builder image for packer images"

ENV GOROOT="/usr/local/go" \
    GOPATH="/go" \
    PATH="/go/bin:/usr/local/go/bin:$PATH"

SHELL ["/bin/bash", "-eufo", "pipefail", "-c"]

COPY --from=installer --chown=root:root \
	/usr/local/bin/* \
	/usr/local/bin/

COPY --from=installer --chown=root:root \
	/tmp/go \
	/usr/local/go

# hadolint ignore=DL3018,SC2016
RUN apk add --no-cache \
		binutils \
		git \
		make \
		musl-dev \
		parallel \
		sed \
	&& ( set +f \
		&& chmod 0755 /usr/local/bin/* ) \
	# install else
	&& echo "done"

CMD ["/bin/bash"]
